// // Write a user defined function named Upper-half() which takes a two dimensional array A, with size N rows and N columns as argument and prints the upper half of the array.
// // e.g.,
// // 2 3 1 5 0                              2 3 1 5 0
// // 7 1 5 3 1                                 1 5 3 1
// // 2 5 7 8 1   Output will be:          7 8 1
// // 0 1 5 0 1                                       0 1
// // 3 4 9 1 5     

// using System;

// namespace dotnetlinux
// {
//     class upperhalf
//     {
//         static void Main(string[] args)
//         {
//             int rows = 5;
//             int columns = 5;


//             int[,] UppHalf = new int[rows, columns];
           

//             UpperHalf(out UppHalf, rows, columns);

//             // display new matrix in 5x5 form
//             /* int i, j;
//             for (i = 0; i < rows; i++)
//             {
//                 for (j = 0; j < columns; j++)
//                     Console.Write("{0}\t", UppHalf[i, j]);
//                 Console.WriteLine();
//             }
//             Console.ReadKey();*/

//         }

//         public static void UpperHalf(out int[,] array, int rows, int columns)
//         {

//             array = new int[rows, columns];

//             Console.WriteLine("Enter elements:");
//             for (int i = 0; i < rows; i++)
//             {
//                 for(int j = 0;j < columns; j++)
//                 {
//                     array[i, j] = Convert.ToInt32(Console.ReadLine());
//                     //Console.Write("{0}\t", array[i, j]);

//                 }
//             }
            
//              for (int i = 0; i < rows; i++)
//             {
//                 for (int j = 0; j < columns; j++)
//                 {
//                     if (i <= j)
//                     {
//                         Console.WriteLine("{0}", array[i,j]);
//                     }
//                     else
//                     {
//                         Console.WriteLine(" \t");
//                     }
//                 }

//             }
//             Console.WriteLine(" \n");
//         }
//     }

// }