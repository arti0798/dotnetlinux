using System;
using System.Collections.Generic;
using System.Text;
namespace dotnetlinux.hw
{
     class MyException : Exception
    {
        public void MyExceptiona()
        {
            Console.WriteLine("Invalid Age");
            
        }
    }
    public class trycatch
    {
        public static void Main(String[] arg)
        {
            int age;
            string name;
            Console.Write("Enter Age: ");
            
            age= Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Age is:" + age);
            Console.WriteLine("Enter name:");
            name = Console.ReadLine();
            Console.WriteLine("Name is: " + name); 
            try
            {
                if (age <18 || age > 60) 
                {
                    throw new MyException();

                }
            }
            catch(MyException e)
            {
                e.MyExceptiona();
                
            }
            Console.WriteLine("My name is {0} and I am {1} years old",name,age);
        }
    }


}