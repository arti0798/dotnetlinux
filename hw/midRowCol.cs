// // //  Write a function in C# which accepts a 2D array of integers and its size 
// // //  Write a function in C# which accepts a 2D array of integers and its size as arguments and displays the elements of middle row and the elements of middle column.
// // // [Assuming the 2D Array to be a square matrix with odd dimension i.e. 3x3, 5x5, 7x7 etc...]
// // // Example, if the array contents is
// // // 3  5  4
// // // 7  6  9
// // // 2  1  8
// // // Output through the function should be :
// // // Middle Row : 7 6 9
// // // Middle column : 5 6 1


// using System;
// namespace dotnetlinux.hw
// {
//     public class midRowCol
//     {
//         static void Main(string[] args)
//         {
           
//                 int rows = 3;
//                 int columns = 3;


//                 int[,] middle = new int[rows, columns];


//                 MiddleRowCol(out middle, rows, columns);

//                 // display new matrix in 5x5 form
//                 /* int i, j;
//                 for (i = 0; i < rows; i++)
//                 {
//                     for (j = 0; j < columns; j++)
//                         Console.Write("{0}\t", UppHalf[i, j]);
//                     Console.WriteLine();
//                 }
//                 Console.ReadKey();*/

//             }

//             public static void MiddleRowCol(out int[,] array, int rows, int columns)
//             {

//                 array = new int[rows, columns];

//                 Console.WriteLine("Enter elements:");
//                 for (int i = 0; i < rows; i++)
//                 {
//                     for (int j = 0; j < columns; j++)
//                     {
//                         array[i, j] = Convert.ToInt32(Console.ReadLine());
//                         //Console.Write("{0}\t", array[i, j]);

//                     }
//                 }

//             Console.WriteLine("Middle Row is: ");   // printing the mid row
//             for (int i = 0; i < rows; i++)
//             {
//                 for (int j = 0; j < columns; j++)
//                 {
//                     if (i == rows / 2)
//                         Console.WriteLine("elements:" + array[i,j]) ;
//                 }


//             }
//             Console.WriteLine("\nMiddle Column is:");    // printing the mid col
//             for (int i = 0; i < rows; i++)
//             {
//                 for (int j = 0; j < columns; j++)
//                 {
//                     if (j == columns / 2)
//                         Console.WriteLine("elements:"+ array[i,j] );
//                 }
//             }
//         }
//     }
// }